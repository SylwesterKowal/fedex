<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Fedex\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager
    )
    {
        $this->objectManager = $objectManager;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }


    protected $storeManager;
    protected $objectManager;




    public function getModeTest($storeId = null)
    {
        return $this->getConfigValue('fedex/test/test', $storeId);
    }
    public function isEnabled($storeId = null)
    {
        return $this->getConfigValue('fedex/general/enable', $storeId);
    }

    public function getApiKey($storeId = null)
    {
        return $this->getConfigValue('fedex/general/apikey', $storeId);
    }

    public function getApiPassword($storeId = null)
    {
        return $this->getConfigValue('fedex/general/password', $storeId);
    }


    public function getApiWsdlProduction($storeId = null)
    {
        return $this->getConfigValue('fedex/general/wsdl', $storeId);
    }
    public function getApiWsdlTest($storeId = null)
    {
        return $this->getConfigValue('fedex/test/wsdl', $storeId);
    }

    public function getApiServiceType($storeId = null)
    {
        return $this->getConfigValue('fedex/general/type', $storeId);
    }




    public function getApiTestKey($storeId = null)
    {
        return $this->getConfigValue('fedex/test/apikey', $storeId);
    }

    public function getApiTestPassword($storeId = null)
    {
        return $this->getConfigValue('fedex/test/password', $storeId);
    }


    public function getApiPersonName($storeId = null)
    {
        return $this->getConfigValue('fedex/shipper/personname', $storeId);
    }

    public function getApiPersonSurname($storeId = null)
    {
        return $this->getConfigValue('fedex/shipper/personsurname', $storeId);
    }
    public function getApiEmail($storeId = null)
    {
        return $this->getConfigValue('fedex/shipper/personemail', $storeId);
    }

    public function getApiCompanyName($storeId = null)
    {
        return $this->getConfigValue('fedex/shipper/companyname', $storeId);
    }

    public function getApiPhoneNumber($storeId = null)
    {
        return $this->getConfigValue('fedex/shipper/phonenumber', $storeId);
    }

    public function getApiStreetLines($storeId = null)
    {
        return $this->getConfigValue('fedex/shipper/streetlines', $storeId);
    }

    public function getApiCity($storeId = null)
    {
        return $this->getConfigValue('fedex/shipper/city', $storeId);
    }

    public function getApiPostalCode($storeId = null)
    {
        return $this->getConfigValue('fedex/shipper/postalcode', $storeId);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getCurrentUrl()
    {
        $model = $this->objectManager->get('Magento\Framework\UrlInterface');
        return $model->getCurrentUrl();
    }

}


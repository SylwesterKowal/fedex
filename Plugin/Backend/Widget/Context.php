<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Fedex\Plugin\Backend\Widget;

class Context
{
    protected $backendUrl;
    protected $_moduleHelper;

    public function __construct(
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Kowal\Fedex\Helper\Data $moduleHelper
    )
    {
        $this->backendUrl = $backendUrl;
        $this->_moduleHelper = $moduleHelper;

    }


    public function afterGetButtonList(
        \Magento\Backend\Block\Widget\Context $subject,
        $buttonList
    )
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $objectManager->get('Magento\Framework\App\Action\Context')->getRequest();
        $order_id = $request->getParam('order_id');
        if ($request->getFullActionName() == 'sales_order_view' /* && $this->_moduleHelper->isEnabled() */) {
            $buttonList->add(
                'custom_button_fedex',
                [
                    'label' => __('FedEx'),
                    'onclick' => 'window.open(\'' . $this->getCustomUrl($order_id) . '\',null,\'width=800,height=800\')',
                    'onclick' => "setLocation('" . $this->getCustomUrl($order_id) . "')",
                    'class' => 'ship'
                ]
            );
        }

        return $buttonList;

    }

    public function getCustomUrl($oId = '26')
    {
        $params = array('order_id' => $oId);
        return $this->backendUrl->getUrl("fedex/index/index", $params);
    }

}


<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Fedex\Model\Config\Source;

class Type implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'zlecenie', 'label' => __('Zlecenie')],['value' => 'list', 'label' => __('List')]];
    }

    public function toArray()
    {
        return ['zlecenie' => __('zlecenie'),'list' => __('list')];
    }
}

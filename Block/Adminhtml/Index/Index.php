<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Fedex\Block\Adminhtml\Index;

class Index extends \Magento\Backend\Block\Widget\Container
{
    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    public $directoryList;
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    public $backendUrl;
    protected $_order;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;
    private $modeTest = true;

    protected $moduleHelper;

    /**
     * @var \Magento\Sales\Model\Order\Shipment\TrackFactory
     */
    protected $trackFactory;

    protected $fileArray;
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;
//    protected $serviceType = 'INTERNATIONAL_PRIORITY';
//    protected $serviceType = 'FEDEX_NEXT_DAY_END_OF_DAY';

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     * @param \Magento\Framework\ObjectManagerInterface $objectManager_
     * @param \Magento\Framework\Filesystem\DirectoryList $directory_
     * @param \Wm21w\Fedex\Helper\Data $moduleHelper_
     * @param \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory_
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Backend\Block\Widget\Context $context, array $data = [],
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\ObjectManagerInterface $objectManager_,
        \Magento\Framework\Filesystem\DirectoryList $directory_,
        \Kowal\Fedex\Helper\Data $moduleHelper_,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory_,
        \Magento\Catalog\Model\ProductRepository $productRepository
    )
    {
        parent::__construct($context, $data);
        $this->backendUrl = $backendUrl;
        $this->objectManager = $objectManager_;
        $this->directoryList = $directory_;
        $this->request = $request;
        $this->moduleHelper = $moduleHelper_;
        $this->modeTest = $this->moduleHelper->getModeTest();
        $this->trackFactory = $trackFactory_;
        $this->productRepository = $productRepository;

//        print_r("__contruct index");

//        $this->fedexInternational = $fedexInternational;
//        $this->fedexInternational->modeTest = $this->modeTest;

        $this->_order = $this->getOrder();
        if (!defined('Newline')) {
            define('Newline', "<br />");
        }
        $this->fedexResponseNumber = 0;

        $this->pupfolder = $this->directoryList->getPath('pub');

        if ($this->modeTest) {
            $this->path_to_wsdl = $this->pupfolder . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'fedex' . DIRECTORY_SEPARATOR . $this->moduleHelper->getApiWsdlTest();
        } else {
            $this->path_to_wsdl = $this->pupfolder . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'fedex' . DIRECTORY_SEPARATOR . $this->moduleHelper->getApiWsdlProduction();
        }

        ini_set("soap.wsdl_cache_enabled", "0");

        $this->client = new \SoapClient($this->path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

    }

    public function testInternational()
    {
        try {
            $this->fedexInternational->executeRequest();
        } catch (\Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    public function getMode()
    {
        return ($this->modeTest) ? 'TEST' : 'PROD';
    }

    public function buildRequestOnePackage()
    {
        $shippingAddress = $this->_order->getShippingAddress();

        $list = [
            'accessCode' => $this->getProperty('password'),
            'shipmentV2' => [
                'nrExt' => null,
                'paymentForm' => 'G', //$this->getProperty('formaPlatnosci'),
                'shipmentType' => 'K',
                'payerType' => 2,
                'sender' => [
                    'senderId' => $this->getProperty('key'),
                    'contactDetails' => [
                        'name' => $this->moduleHelper->getApiPersonName(),
                        'surname' => $this->moduleHelper->getApiPersonSurname(),
                        'phoneNo' => $this->moduleHelper->getApiPhoneNumber(),
                        'email' => $this->moduleHelper->getApiEmail(),
                    ]
                ],
                'receiver' => $this->addRecipient(),


                'payer' => [
                    'payerId' => null,
                    'contactDetails' => [
                        'name' => null,
                        'surname' => null,
                        'phoneNo' => null,
                        'email' => null
                    ]
                ],
                'proofOfDispatch' => [
                    'senderSignature' => $this->moduleHelper->getApiPersonName() . ' ' . $this->moduleHelper->getApiPersonSurname(),
                    'courierId' => '00000',
                    'sendDate' => $this->isWeekend(date('Y-m-d')) . " 11:00"
                ],
                'cod' => [
                    'codType' => null,
                    'codValue' => null,
                    'bankAccountNumber' => null
                ],
                'insurance' => [
                    'insuranceValue' => null,
                    'contentDescription' => null
                ],
                'additionalServices' => null,
                'parcels' => [
                    'parcel' => [
                        'type' => 'PC',
                        'weight' => $this->getProductsWeight(),
                        'shape' => 0
                    ]
                ],
                'remarks' => ""
            ]
        ];


        // zlecenie
        $zlecenie = [
            'kodDostepu' => $this->getProperty('password'),
            'zleceniePodjecia' => [
                'dataOdbioru' => $this->isWeekend(date('Y-m-d')),
                'godzinaOdbioru' => '',
                'formaPlatnosci' => $this->getProperty('formaPlatnosci'),
                'placi' => 1,
                'rodzajPrzesylki' => 'K',
                'platnikNrKlienta' => $this->getProperty('key'),
                'platnikTelKontakt' => $this->moduleHelper->getApiPhoneNumber(),
                'nazwaFirmy' => trim($shippingAddress->getCompany()),
                'imie' => $shippingAddress->getFirstname(),
                'nazwisko' => $shippingAddress->getLastname(),
                'miasto' => $shippingAddress->getCity(),
                'kod' => strtoupper($shippingAddress->getPostcode()),
                'ulica' => implode(', ', $shippingAddress->getStreet()),
                'nrDom' => null,
                'nrLokal' => null,
                'telKontakt' => $shippingAddress->getTelephone(),
                'emailKontakt' => $this->_order->getCustomerEmail(),
                'iloscPaczek' => 1,
                'wagaPaczekKg' => $this->getProductsWeight(),
                'uwagi' => ''
            ]
        ];

        switch ($this->moduleHelper->getApiServiceType()) {
            case "list":
                return $list;
                break;
            case "zlecenie":
                return $zlecenie;
                break;
        }
    }

    /**
     * @param $date
     * @return false|string
     */
    function isWeekend($date)
    {
        if (date('N', strtotime($date)) >= 6) {
            $nextTuesday = strtotime('next monday');
            return date('Y-m-d', $nextTuesday);
        } else {
            return $date;
        }
    }

    public function buildRequestOnePackage_old()
    {
        // jakaś dokumentacja w pdf:
        // https://www.fedex.com/templates/components/apps/wpor/secure/downloads/pdf/201707/FedEx_WebServices_ShipService_WSDLGuide_v2017.pdf

        $request = array();

        $request['WebAuthenticationDetail'] = array(
            'ParentCredential' => array(
                'Key' => $this->getProperty('parentkey'),
                'Password' => $this->getProperty('parentpassword')
            ),
            'UserCredential' => array(
                'Key' => $this->getProperty('key'),
                'Password' => $this->getProperty('password')
            )
        );

        $request['ClientDetail'] = array(
            'AccountNumber' => $this->getProperty('shipaccount'),
            'MeterNumber' => $this->getProperty('meter')
        );

        $request['TransactionDetail'] = array('CustomerTransactionId' => $this->_order->getIncrementId());
        $request['Version'] = array(
            'ServiceId' => 'ship',
            'Major' => '21',
            'Intermediate' => '0',
            'Minor' => '0'
        );
        $request['RequestedShipment'] = array(
            'ShipTimestamp' => date('c'),
            'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
            'ServiceType' => $this->getServiceType(), // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
//            'ServiceType' => $this->moduleHelper->getApiServiceType(), // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
            'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
            'Shipper' => $this->addShipper(),
            'Recipient' => $this->addRecipient(),
            'ShippingChargesPayment' => $this->addShippingChargesPayment(),
            'CustomsClearanceDetail' => $this->addCustomClearanceDetail(),
//            'SpecialServicesRequested' => $this->addSpecialServices(),
            'LabelSpecification' => $this->addLabelSpecification(),
            'CustomerSpecifiedDetail' => array(
                'MaskedData' => 'SHIPPER_ACCOUNT_NUMBER'
            ),
        );

        $request['RequestedShipment']['PackageCount'] = 1;
        $request['RequestedShipment']['RequestedPackageLineItems'] = array(
            '0' => $this->addPackageLineItem1()
        );
        $request['RequestedShipment']['CustomerReferences'] = array(
            '0' => array(
                'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
                'Value' => 'TC007_07_PT1_ST01_PK01_SNDUS_RCPCA_POS'
            )
        );

        // Identifies the total weight of the shipment being conveyed to
        //FedEx. This is only applicable to International shipments and
        //should only be used on the first package of a multiple-package
        //shipment.
        $request['RequestedShipment']['TotalWeight'] = array(
            'Units' => 'KG',
            'Value' => $this->getProductsWeight()
        );

        return $request;
    }

    protected function getServiceType()
    {
        $shippingAddress = $this->_order->getShippingAddress();
        $countryCode = $shippingAddress->getCountryId();

        if ($countryCode == 'GB') {
            return 'FEDEX_NEXT_DAY_END_OF_DAY';
        } else {
//            return 'INTERNATIONAL_PRIORITY'; // priority jest drogieeee
            return 'INTERNATIONAL_ECONOMY';
        }
    }

    protected function getPackageCount()
    {
        return (int)$this->request->getParam('package_count');
    }

    public function shipToFedex()
    {
//        $rateRequestTypes = 'NONE';
//        $rateRequestTypes = 'LIST';
        $rateRequestTypes = 'PREFERRED';

        $packageCount = $this->getPackageCount();
        if ($packageCount == 1) {
            $request = $this->buildRequestOnePackage();
            $this->shipToFedexRequest($request, true);
            echo "</br>";
            echo 'Kliknij w link <a href="' . $this->fileArray[0] . '" target="_blank">Wydruk etykiety ' . $this->fedexResponseNumber . '</a> aby pobrać plik PDF';
        } else {
            try {

                foreach (range(1, $packageCount) as $i) {
                    $request = $this->buildRequestOnePackage();
                    $this->shipToFedexRequest($request, true);
                    echo "</br>";
                    echo 'Kliknij w link <a href="' . $this->fileArray[0] . '" target="_blank">Wydruk etykiety ' . $this->fedexResponseNumber . '</a> aby pobrać plik PDF';
                }

            } catch (\Exception $ex) {
                print_r($ex->getMessage());
            }

        }
    }


    private function mergePDF()
    {
        echo "<table class=\"tabela-pdf\"><tr><th>Version Separate</th><th>Version 1 connected</th><th>Version 2 connected</th></tr>";
        try {
            echo "<tr><td>";
            $i = 1;
            foreach ($this->fileArray as $file) {
                echo $i . '. Label <a href="' . $file . '" target="_blank">to print</a> was generated. <br/>';
                $i++;
            }
            echo "</td><td>";
            $datadir = DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR;
            $fileName1 = "1-" . uniqid() . date('Ymd-His') . ".pdf";
            $fileName2 = "2-" . uniqid() . date('Ymd-His') . ".pdf";
            $outputName1 = $this->pupfolder . $datadir . $fileName1;
            $outputName2 = $this->pupfolder . $datadir . $fileName2;

            $cmd1 = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName1 ";
            $cmd2 = "convert ";
            //Add each pdf file to the end of the command
            foreach ($this->fileArray as $file) {
                $cmd1 .= $this->pupfolder . $file . " ";
                $cmd2 .= $this->pupfolder . $file . " ";
            }
            $cmd2 .= $outputName2;
            $result1 = shell_exec($cmd1);
            $result2 = shell_exec($cmd2);

            echo 'Label <a href="' . $datadir . $fileName1 . '" target="_blank">to print</a> was generated.';
            echo "</td><td>";
            echo 'Label <a href="' . $datadir . $fileName2 . '" target="_blank">to print</a> was generated.';
            echo "</td></tr></table>";
            echo "<style>.tabela-pdf td { padding: 20px; }</style>";
        } catch (\Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    public function shipToFedexRequest($request, $saveShipmentTruckNumberToOrder)
    {
        //The WSDL is not included with the sample code.
        //Please include and reference in $path_to_wsdl variable.

        $labelPathForWrite = $this->directoryList->getPath('pub') . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . 'shipexpresslabel_' . $this->_order->getIncrementId() . '-' . ($this->fedexResponseNumber) . '.pdf';
        $labelPathForBackendLink = $this->isPubRoot() . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . 'shipexpresslabel_' . $this->_order->getIncrementId() . '-' . ($this->fedexResponseNumber) . '.pdf';

        $this->fileArray[] = $labelPathForBackendLink;

        try {

//             // FedEx web service invocation
            // FedEx web service invocation

//            var_dump($response);

            $this->fedexResponseNumber++;
            echo '<p style="padding-bottom:6px;">';
            echo "<u>Numer paczki: {$this->fedexResponseNumber}</u><br />";


            switch ($this->moduleHelper->getApiServiceType()) {
                case "zlecenie":

                    $response = $this->client->dodajZlecenie($request);

                    if ($response->fdsZlecenie->zleId != '') {
                        echo __("Zlecenie wykonane pomyślnie");
                    } else {
                        echo __("Błąd w realizacji zleceniea. ") . print_r($response, true);
                    }
                    break;
                case "list":

                    $response = $this->client->zapiszListV2($request);
//                    var_dump($response);
                    // list
                    // object(stdClass)#2260 (1) { ["przesylkaZapisanaV2"]=> object(stdClass)#2259 (12) { ["waybill"]=> string(13) "6231555489369" ["paymentForm"]=> string(1) "G" ["shipmentType"]=> string(1) "K" ["payerType"]=> string(1) "2" ["sender"]=> object(stdClass)#2250 (2) { ["senderId"]=> string(9) "546413173" ["contactDetails"]=> object(stdClass)#2262 (4) { ["name"]=> string(9) "Sylwester" ["surname"]=> string(5) "Kowal" ["phoneNo"]=> string(12) "+48608012047" ["email"]=> string(22) "sylwester.kowal@21w.pl" } } ["receiver"]=> object(stdClass)#2258 (2) { ["receiverId"]=> string(9) "557138898" ["addressDetails"]=> object(stdClass)#2249 (7) { ["isCompany"]=> string(1) "1" ["companyName"]=> string(6) "21W.PL" ["city"]=> string(10) "STRZYŻÓW" ["postalCode"]=> string(6) "38-100" ["street"]=> string(15) "DOBRZECHÓW 509" ["homeNo"]=> string(1) "." ["localNo"]=> string(0) "" } } ["proofOfDispatch"]=> object(stdClass)#2268 (3) { ["senderSignature"]=> string(15) "Sylwester Kowal" ["courierId"]=> string(5) "00000" ["sendDate"]=> string(16) "2020-11-09 11:00" } ["cod"]=> object(stdClass)#2256 (0) { } ["insurance"]=> object(stdClass)#2267 (1) { ["insuranceValue"]=> string(1) "0" } ["additionalServices"]=> object(stdClass)#2266 (0) { } ["parcels"]=> object(stdClass)#2265 (1) { ["parcel"]=> object(stdClass)#2264 (8) { ["waybill"]=> string(13) "6231555489369" ["type"]=> string(2) "PC" ["weight"]=> string(2) "32" ["dim1"]=> string(1) "0" ["dim2"]=> string(1) "0" ["dim3"]=> string(1) "0" ["shape"]=> string(1) "0" ["dimWeight"]=> string(5) "0,000" } } ["remarks"]=> string(0) "" } }

                    if ($response->przesylkaZapisanaV2->waybill != '') {

                        if ($saveShipmentTruckNumberToOrder) {
                            $CarrierCode = "FEDEX";
                            $TrackingNumber = $response->przesylkaZapisanaV2->waybill;
                            $this->saveShipmentTruckNumberToOrder($CarrierCode, $TrackingNumber);
                        }

                        // zmiana statusu zamowienia
                        $shippingAddress = $this->_order->getShippingAddress();
                        $countryId = $shippingAddress->getCountryId();
//                if($countryId != 'GB') {
//                    $this->_order->setState(\Magento\Sales\Model\Order::STATE_COMPLETE);
//                    $this->_order->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
//                    $this->_order->save();
//                }

                        $request_ = [
                            'kodDostepu' => $this->getProperty('password'),
                            'numerPrzesylki' => $response->przesylkaZapisanaV2->waybill,
                            'format' => 'epl'
                        ];

                        $request_ = [
                            'kodDostepu' => $this->getProperty('password'),
                            'numerPaczki' => $response->przesylkaZapisanaV2->waybill,
                            'format' => 'PDF'
                        ];


                        $response_ = $this->client->wydrukujEtykietePaczki($request_);

                        file_put_contents($labelPathForWrite, $response_->etykietaBajty);
                        return $response;
                    } else {
                        echo __("Error:") . " " . print_r($response, true);
                    }
                    break;

            }
            echo '</p>';
        } catch (\SoapFault $exception) {
            $this->printFault($exception, $this->client);
            echo '</p>';
            return null;
        }
    }

    private function isPubRoot()
    {
        $pub = $this->directoryList->getUrlPath("pub");
        if ($pub == "pub") {
            return "/pub";
        } else {
            //pub directory is being pointed
            return "";
        }
    }

    public function getRequestXml()
    {
        $xml = $this->client->__getLastRequest();
        $dom = new \DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xml);
        $dom->formatOutput = TRUE;
        return $dom->saveXML();
    }

    public function getResponseXml()
    {
        $xml = $this->client->__getLastResponse();
        $dom = new \DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xml);
        $dom->formatOutput = TRUE;
        return $dom->saveXML();
    }


    function addShipper()
    {
        $shipper = array(
            'Contact' => array(
                'PersonName' => $this->moduleHelper->getApiPersonName(),
                'CompanyName' => $this->moduleHelper->getApiCompanyName(),
                'PhoneNumber' => $this->moduleHelper->getApiPhoneNumber()
            ),
            'Address' => array(
                'StreetLines' => $this->moduleHelper->getApiStreetLines(),
                'City' => $this->moduleHelper->getApiCity(),
                'StateOrProvinceCode' => '',
                'PostalCode' => strtoupper($this->moduleHelper->getApiPostalCode()),
                'CountryCode' => 'GB'
            )
        );
        return $shipper;
    }


    function addRecipient()
    {
        $shippingAddress = $this->_order->getShippingAddress();

        $klient = [
            'kodDostepu' => $this->getProperty('password'),
            'klient' => [
                'numer' => null,
                'nrExt' => null,
                'nazwa' => trim($shippingAddress->getCompany()),
                'czyFirma' => ((empty($shippingAddress->getCompany())) ? 0 : 1),
                'nip' => $this->_order->getData('customer_taxvat'),
                'imie' => $shippingAddress->getFirstname(),
                'nazwisko' => $shippingAddress->getLastname(),
                'miasto' => $shippingAddress->getCity(),
                'kod' => $shippingAddress->getPostcode(),
                'kodKraju' => null,
                'ulica' => implode(', ', $shippingAddress->getStreet()),
                'nrDom' => 0,
                'nrLokal' => null,
                'telKontakt' => $shippingAddress->getTelephone(),
                'emailKontakt' => $this->_order->getCustomerEmail(),
                'czyNadawca' => 0,
                'fax' => null,
                'telefonKom' => $shippingAddress->getTelephone(),
            ]
        ];
        $nKlient = $this->client->dodajKlienta($klient);
        // object(stdClass)#2228 (1) { ["klientZapisany"]=> object(stdClass)#2249 (11) { ["numer"]=> string(9) "557138899" ["nazwa"]=> string(6) "21W.PL" ["czyFirma"]=> string(1) "1" ["miasto"]=> string(10) "STRZYŻÓW" ["kod"]=> string(6) "38-100" ["ulica"]=> string(15) "DOBRZECHÓW 509" ["nrDom"]=> string(1) "0" ["telKontakt"]=> string(12) "+48608012047" ["emailKontakt"]=> string(22) "SYLWESTER.KOWAL@21W.PL" ["czyNadawca"]=> string(1) "0" ["telefonKom"]=> string(12) "+48608012047" } }

        return $nowyKlient = [
            'receiverId' => $nKlient->klientZapisany->numer,
            'addressDetails' => [
                'isCompany' => ((empty($shippingAddress->getCompany())) ? 0 : 1),
                'companyName' => trim($shippingAddress->getCompany()),
                'vatNo' => $this->_order->getData('customer_taxvat'),
                'name' => $shippingAddress->getFirstname(),
                'surname' => $shippingAddress->getLastname(),
                'nrExt' => null,
                'city' => $shippingAddress->getCity(),
                'postalCode' => strtoupper($shippingAddress->getPostcode()),
                'countryCode' => 'PL',
                'street' => implode(', ', $shippingAddress->getStreet()),
                'homeNo' => null,
                'localNo' => null
            ],
            'contactDetails' => [
                'name' => $shippingAddress->getFirstname(),
                'surname' => $shippingAddress->getLastname(),
                'phoneNo' => $shippingAddress->getTelephone(),
                'email' => $this->_order->getCustomerEmail()
            ]
        ];
    }

    function addShippingChargesPayment()
    {
        $shippingChargesPayment = array('PaymentType' => 'SENDER',
            'Payor' => array(
                'ResponsibleParty' => array(
                    'AccountNumber' => $this->getProperty('billaccount'),
                    'Contact' => null,
                    'Address' => array(
                        'CountryCode' => 'GB'
                    )
                )
            )
        );
        return $shippingChargesPayment;
    }

    function addLabelSpecification()
    {
        $labelSpecification = array(
            'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
            'ImageType' => 'PDF',  // valid values DPL, EPL2, PDF, ZPLII and PNG
            'LabelStockType' => 'PAPER_4X6', //'PAPER_4X8' //'PAPER_7X4.75'
        );
        return $labelSpecification;
    }

    function addSpecialServices()
    {
        $specialServices = array(
//            'ShipmentSpecialServiceType' => array('PENDING_SHIPMENT'),
//            'SpecialServiceTypes' => array('PENDING_SHIPMENT')
//            'CodDetail' => array(
//                'CodCollectionAmount' => array(
//                    'Currency' => 'UKL',
//                    'Amount' => 100.00
//                ),
//                'CollectionType' => 'ANY' // ANY, GUARANTEED_FUNDS
//            )
        );
        return $specialServices;
    }

    function addCustomClearanceDetail()
    {

        $total = $this->_order->getGrandTotal() - $this->_order->getShippingAmount() - $this->_order->getShippingTaxAmount();

        if ($total < 0) {
            throw new \Exception("Total < 0: $total");
        }

        $customerClearanceDetail = array(
            'DutiesPayment' => array(
                'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
                'Payor' => array(
                    'ResponsibleParty' => array(
                        'AccountNumber' => $this->getProperty('dutyaccount'),
                        'Contact' => null,
                        'Address' => array(
                            'CountryCode' => 'GB'
                        )
                    )
                )
            ),
            'DocumentContent' => 'NON_DOCUMENTS',
            'CustomsValue' => array(
                'Currency' => 'UKL',
                'Amount' => $total
            ),
            'Commodities' => $this->addCommodities(),
            'ExportDetail' => array(
                'B13AFilingOption' => 'NOT_REQUIRED'
            )
        );
        return $customerClearanceDetail;
    }

    private function addCommodities()
    {
        $commodities = [];
        $items = $this->_order->getAllVisibleItems();
        $l = 1;
        $item_sum = 0;
        foreach ($items as $i) {
            $commodities[] = array(
                'NumberOfPieces' => $l,
                'Description' => $i->getName() . ' ' . $i->getSku(),
                'CountryOfManufacture' => 'GB',
                'Weight' => array(
                    'Units' => 'KG',
                    'Value' => $i->getWeight()
                ),
                'Quantity' => $i->getQtyOrdered(),
                'QuantityUnits' => 'EA',
                'UnitPrice' => array(
                    'Currency' => 'UKL',
                    'Amount' => $i->getPrice()
                ),
                'CustomsValue' => array(
                    'Currency' => 'UKL',
                    'Amount' => $i->getRowTotalInclTax()
                )
            );
            $l++;

        }
        return $commodities;
    }

    private function addPackageLineItem1()
    {
        $packageLineItem = array(
            'SequenceNumber' => 1,
            'GroupPackageCount' => 1,
            'Weight' => array(
                'Value' => $this->getProductsWeight(),
                'Units' => 'KG'
            ),
            'Dimensions' => array(
                'Length' => 20,
                'Width' => 20,
                'Height' => 10,
                'Units' => 'CM'
            )
        );
        return $packageLineItem;
    }

    /*========================= Order =====================================*/

    public function getOrder()
    {
        return $this->objectManager->create('Magento\Sales\Model\Order')->load($this->request->getParam('order_id'));
    }

    public function getCustomUrl()
    {
        $params = array('order_id' => $this->request->getParam('order_id'));
        return $this->backendUrl->getUrl("sales/order/view", $params);
    }


    private function getProductsWeight()
    {
        $weight = 0;
        foreach ($this->_order->getAllItems() as $item) {
            if ($hasParent = $item->getParentItemId()) {
                continue;
            }
            $weight = $weight + $item->getWeight();
        }
        if ($weight > 0) {
            return $weight;
        } else {
            return 1;
        }
    }

    public function getProductsTable()
    {
        $table = '';

        $table .= '<table id="fedex-table" style="width: 100%; padding: 20px" cellpadding="10"><thead><tr><th>Image</th><th>Sku</th><th>Name</th><th>Qty</th><th>Weight</th></tr></thead>';
        $table .= '<tbody>';
        $tQty = 0;
        $tWeight = 0;
        foreach ($this->_order->getAllItems() as $item) {
            if ($hasParent = $item->getParentItemId()) {
                continue;
            }


            $table .= '<tr><td class="td-img">';

            try {
                $product = $this->productRepository->get($item->getSku())->setStoreId(0);
                $pub = $this->isPubRoot();
                $table .= '<img src="' . $pub . '/media/catalog/product' . $product->getImage() . '" style="width:50px;" title="' . $item->getName() . '" alt="' . $item->getName() . '"/>';
            } catch (\Exception $ex) {
                $table .= '???';
            }

//            $table .= '<tr><td>';


            $table .= '</td><td class="td-text">' . $item->getSku() . '</td><td class="td-text">' . $item->getName() . '</td><td class="td-text" style="text-align: right;">' . round($item->getQtyOrdered(), 1) . '</td><td class="td-text" style="text-align: right;">' . round($item->getWeight() * $item->getQtyOrdered(), 1) . '</td></tr>';
            $tWeight = round($tWeight + ($item->getWeight() * $item->getQtyOrdered()), 1);
            $tQty = round($tQty + $item->getQtyOrdered(), 1);
        }
        $table .= '<tr class="tr-total"><td class="td-total" colspan="3">Total</td><td style="text-align: right">' . $tQty . '</td><td style="text-align: right;">' . $tWeight . '</td></tr></tbody></table>';
        return $table;
    }

    /*=======================  LIB FedEx  ==================================*/


    function saveShipmentTruckNumberToOrder($CarrierCode, $TrackingNumber)
    {
        if ($this->_order->canShip()) {
            // Initialize the order shipment object
            $convertOrder = $this->objectManager->create('Magento\Sales\Model\Convert\Order');
            $shipment = $convertOrder->toShipment($this->_order);
            // Loop through order items
            foreach ($this->_order->getAllItems() as $orderItem) {
                // Check if order item has qty to ship or is virtual
                if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                    continue;
                }

                $qtyShipped = $orderItem->getQtyToShip();

                // Create shipment item with qty
                $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

                // Add shipment item to shipment
                $shipment->addItem($shipmentItem);
                $shipment->addItem($shipmentItem);
            }


            // Register shipment
            $shipment->register();


            $shipment->getOrder()->setIsInProcess(true);

            try {
                // Save created shipment and order
                $data = array(
                    'carrier_code' => $CarrierCode,
                    'title' => 'FedEx',
                    'number' => $TrackingNumber
                );

                $track = $this->trackFactory->create()->addData($data);
                $shipment->addTrack($track)->save();

                $shipment->getOrder()->save();

                // Send email
//                $this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
//                    ->notify($shipment);

                $shipment->save();
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }

        }
    }

    function printRequestResponse($client)
    {
        echo '<h2>Request</h2>' . "\n";
        echo '<pre>' . htmlspecialchars($client->__getLastRequest()) . '</pre>';
        echo "\n";

        echo '<h2>Response</h2>' . "\n";
        echo '<pre>' . htmlspecialchars($client->__getLastResponse()) . '</pre>';
        echo "\n";
    }

    /**
     *  Print SOAP Fault
     */
    function printFault($exception, $client)
    {
        echo '<h2>Fault</h2>' . "<br>\n";
        echo "<b>Code:</b>{$exception->faultcode}<br>\n";
        echo "<b>String:</b>{$exception->faultstring}<br>\n";


        echo '<h2>Request</h2>' . "\n";
        echo '<pre>' . htmlspecialchars($client->__getLastRequest()) . '</pre>';
        echo "\n";
    }


    /**
     * This section provides a convenient place to setup many commonly used variables
     * needed for the php sample code to function.
     * > https://ws.fedex.com:443/web-services
     *
     * > Password: iTmQm7Sz0odcGCOzQx4foHChQ
     *
     * > FedEx Account Number: 348140159
     *
     * > FedEx Web Services Meter Number: 111270699
     *
     */
    function getProperty($var)
    {

        if ($var == 'key' && $this->modeTest) return $this->moduleHelper->getApiTestKey();
        if ($var == 'key' && !$this->modeTest) return $this->moduleHelper->getApiKey();
        if ($var == 'password' && $this->modeTest) return $this->moduleHelper->getApiTestPassword();
        if ($var == 'password' && !$this->modeTest) return $this->moduleHelper->getApiPassword();
        if ($var == 'formaPlatnosci' && $this->modeTest) return "G";
        if ($var == 'formaPlatnosci' && !$this->modeTest) return "P";
        if ($var == 'formaPlatnosci') return 'XXX';
        if ($var == 'parentpassword') return 'XXX';
        if ($var == 'freightaccount') return 'XXX';
        if ($var == 'trackaccount') return 'XXX';
        if ($var == 'dutiesaccount') return 'XXX';
        if ($var == 'importeraccount') return 'XXX';
        if ($var == 'brokeraccount') return 'XXX';
        if ($var == 'distributionaccount') return 'XXX';
        if ($var == 'locationid') return 'PLBA';
        if ($var == 'printlabels') return true;
        if ($var == 'printdocuments') return true;
        if ($var == 'packagecount') return '4';
        if ($var == 'validateaccount') return 'XXX';
        if ($var == 'meter' && $this->modeTest) return $this->moduleHelper->getApiTestMeterNumber();
        if ($var == 'meter' && !$this->modeTest) return $this->moduleHelper->getApiMeterNumber();

        if ($var == 'shiptimestamp') return mktime(10, 0, 0, date("m"), date("d") + 1, date("Y"));

        if ($var == 'spodshipdate') return '2016-04-13';
        if ($var == 'serviceshipdate') return '2013-04-26';
        if ($var == 'shipdate') return '2016-04-21';

        if ($var == 'readydate') return '2014-12-15T08:44:07';
        //if($var == 'closedate') Return date("Y-m-d");
        if ($var == 'closedate') return '2016-04-18';
        if ($var == 'pickupdate') return date("Y-m-d", mktime(8, 0, 0, date("m"), date("d") + 1, date("Y")));
        if ($var == 'pickuptimestamp') return mktime(8, 0, 0, date("m"), date("d") + 1, date("Y"));
        if ($var == 'pickuplocationid') return 'SQLA';
        if ($var == 'pickupconfirmationnumber') return '1';

        if ($var == 'dispatchdate') return date("Y-m-d", mktime(8, 0, 0, date("m"), date("d") + 1, date("Y")));
        if ($var == 'dispatchlocationid') return 'NQAA';
        if ($var == 'dispatchconfirmationnumber') return '4';

        if ($var == 'tag_readytimestamp') return mktime(10, 0, 0, date("m"), date("d") + 1, date("Y"));
        if ($var == 'tag_latesttimestamp') return mktime(20, 0, 0, date("m"), date("d") + 1, date("Y"));

        if ($var == 'expirationdate') return date("Y-m-d", mktime(8, 0, 0, date("m"), date("d") + 15, date("Y")));
        if ($var == 'begindate') return '2014-10-16';
        if ($var == 'enddate') return '2014-10-16';

        if ($var == 'trackingnumber') return 'XXX';

        if ($var == 'hubid') return '5531';

        if ($var == 'jobid') return 'XXX';

        if ($var == 'searchlocationphonenumber') return '5555555555';
        if ($var == 'customerreference') return '39589';

        if ($var == 'shipper') return array(
            'Contact' => array(
                'PersonName' => $this->moduleHelper->getApiPersonName(),
                'CompanyName' => $this->moduleHelper->getApiCompanyName(),
                'PhoneNumber' => $this->moduleHelper->getApiPhoneNumber(),
            ),
            'Address' => array(
                'StreetLines' => array($this->moduleHelper->getApiStreetLines()),
                'City' => $this->moduleHelper->getApiCity(),
                'StateOrProvinceCode' => '',
                'PostalCode' => strtoupper($this->moduleHelper->getApiPostalCode()),
                'CountryCode' => 'GB',
                'Residential' => 1
            )
        );
        if ($var == 'recipient') return array(
            'Contact' => array(
                'PersonName' => 'Recipient Name',
                'CompanyName' => 'Recipient Company Name',
                'PhoneNumber' => '1234567890'
            ),
            'Address' => array(
                'StreetLines' => array('Address Line 1'),
                'City' => 'Bristol',
                'StateOrProvinceCode' => '',
                'PostalCode' => '20171',
                'CountryCode' => 'GB',
                'Residential' => 1
            )
        );

        if ($var == 'address1') return array(
            'StreetLines' => array('10 Fed Ex Pkwy'),
            'City' => 'Memphis',
            'StateOrProvinceCode' => 'TN',
            'PostalCode' => '38115',
            'CountryCode' => 'UKL'
        );
        if ($var == 'address2') return array(
            'StreetLines' => array('13450 Farmcrest Ct'),
            'City' => 'Herndon',
            'StateOrProvinceCode' => 'VA',
            'PostalCode' => '20171',
            'CountryCode' => 'US'
        );
        if ($var == 'searchlocationsaddress') return array(
            'StreetLines' => array('Vitcas Ltd'),
            'City' => 'Bristol',
            'StateOrProvinceCode' => '',
            'PostalCode' => 'BS4 5NZ',
            'CountryCode' => 'GB'
        );

        if ($var == 'shippingchargespayment') return array(
            'PaymentType' => 'SENDER',
            'Payor' => array(
                'ResponsibleParty' => array(
                    'AccountNumber' => $this->getProperty('billaccount'),
                    'Contact' => null,
                    'Address' => array('CountryCode' => 'GB')
                )
            )
        );
        if ($var == 'shipperbilling') return array(
            'Contact' => array(
                'ContactId' => 'freight1',
                'PersonName' => 'Big Shipper',
                'Title' => 'Manager',
                'CompanyName' => 'Freight Shipper Co',
                'PhoneNumber' => '1234567890'
            ),
            'Address' => array(
                'StreetLines' => array(
                    '1202 Chalet Ln',
                    'Do Not Delete - Test Account'
                ),
                'City' => 'Bristol',
                'StateOrProvinceCode' => '',
                'PostalCode' => 'BS4 5NZ',
                'CountryCode' => 'GB    '
            )
        );

        if ($var == 'freightbilling') return array(
            'Contact' => array(
                'ContactId' => 'freight1',
                'PersonName' => 'Big Shipper',
                'Title' => 'Manager',
                'CompanyName' => 'Freight Shipper Co',
                'PhoneNumber' => '1234567890'
            ),
            'Address' => array(
                'StreetLines' => array(
                    '1202 Chalet Ln',
                    'Do Not Delete - Test Account'
                ),
                'City' => 'Bristol',
                'StateOrProvinceCode' => '',
                'PostalCode' => 'BS4 5NZ',
                'CountryCode' => 'GB'
            )
        );
    }

    function setEndpoint($var)
    {
        if ($var == 'changeEndpoint') return false;
        if ($var == 'endpoint') return 'XXX';
    }

    function printNotifications($response)
    {

        echo __("Numer zlecenia") . ": " . $response->fdsZlecenie->zleId . Newline;

        echo Newline;
    }

    function trackDetails($details, $spacer)
    {
        foreach ($details as $key => $value) {
            if (is_array($value) || is_object($value)) {
                $newSpacer = $spacer . '&nbsp;&nbsp;&nbsp;&nbsp;';
                echo '<tr><td>' . $spacer . $key . '</td><td>&nbsp;</td></tr>';
                $this->trackDetails($value, $newSpacer);
            } elseif (empty($value)) {
                echo '<tr><td>' . $spacer . $key . '</td><td>' . $value . '</td></tr>';
            } else {
                echo '<tr><td>' . $spacer . $key . '</td><td>' . $value . '</td></tr>';
            }
        }
    }
}



# Mage2 Module Kowal Fedex

    ``kowal/module-fedex``

 - [FedEx](#markdown-header-main-fedex)
 - [Instalacja](#markdown-header-instalacja)
 - [Konfigurcja](#markdown-header-konfigurcja)
 - [Instrukcja](#markdown-header-instrukcja)
 - [Kurier](#markdown-header-kurier)


## FedEx
Integracja z Magento z FedEx oraz niestandardowa metoda wysyłki FedEx

## Instalacja
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_Fedex`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer
- Udostępnij moduł w repozytorium kompozytorów, na przykład:
    - repozytorium prywatne `repo.magento.com`
    - repozytorium publiczne `packagist.org`
    - publiczne repozytorium github jako vcs
 - Dodaj repozytorium kompozytora do konfiguracji, uruchamiając `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 -
Zainstaluj moduł Composer, uruchamiając `composer require kowal/module-fedex`
 - włącz moduł, uruchamiając `php bin/magento module:enable Kowal_Fedex`
 - zastosuj aktualizacje bazy danych, uruchamiając `php bin/magento setup:upgrade`\*
 - Opróżnij pamięć podręczną, uruchamiając `php bin/magento cache:flush`


## Konfigurcja

 - włącz tryb testowy lub produkcyjny

 - Wypełnij wymagane pola w formularzu

 - Dodaj piki WSDL. Pobierz plik testowy na dysk lokalny: https://test.poland.fedex.com/fdsWs/IklServicePort?WSDL
   i zapisz go z rozszerzeniem „wsdl”
   
 - Podobnie załaduj plik produkcyjny WSDL, który otrzymasz od firmy FEDEX


## Instrukcja

 - Przejdź do listy zamówień i wybierz zamówienie dla którego chesz wykonać list przewozowy.
	- Orders > View > FedEx

 - Ustal ilość paczek i kliknj w przycisk "Wyślij do FedEx"
	- Po poprawnym wysłaniu danych na stronie pojawi się link do wyddruku etykiety




## Kurier

 - Przejdź do obsługi metod wysyłek i włącz metodę FedEx zainstalowaną wraz z tym modułem
	- ustaw koszty wysyłki i zakres krajów dla których będzie dostępna.
